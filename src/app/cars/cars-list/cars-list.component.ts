import {Component, OnInit} from '@angular/core';
import {Car} from '../models/car';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.less']
})
export class CarsListComponent implements OnInit {
  cars: Car[] = [{
    id: 1,
    model: 'Mazda Rx7',
    plate: 'GD2121E',
    deliveryDate: '21-04-2016',
    deadline: '05-05-2016',
    client: {
      firstName: 'Jan',
      surname: 'Kowalski'
    },
    cost: 300,
    isFullyDamaged: true
  },
    {
      id: 2,
      model: 'Mazda Rx7',
      plate: 'GD2121E',
      deliveryDate: '21-04-2017',
      deadline: '05-05-2016',
      client: {
        firstName: 'Jan',
        surname: 'Kowalski'
      },
      cost: 300,
      isFullyDamaged: false
    },
    {
      id: 3,
      model: 'Mazda Rx7',
      plate: 'GD2121E',
      deliveryDate: '21-04-2017',
      deadline: '05-05-2016',
      client: {
        firstName: 'Jan',
        surname: 'Kowalski'
      },
      cost: 300,
      isFullyDamaged: true
    }
  ];

  totalCost: number = this.countTotalCost();
  grossCost: number;
  constructor() {
  }

  ngOnInit(): void {
  }

  countTotalCost(): number {
    return this.cars
      .map((car) => car.cost)
      .reduce((prev, next) => prev + next);
  }

  onShowGross(grossCost: number): void {
    this.grossCost = grossCost;
  }
}
